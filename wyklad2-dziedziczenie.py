class Samochod:
    def __init__(self, kolor, marka):
        self.kolor = kolor
        self.marka = marka

    def start(self):
        if self.czy_moge_wystartowac():
            print("Odpalam", self.marka, "koloru", self.kolor)
        else:
            print("Brak paliwa")

class SamochodElektryczny(Samochod):
    def __init__(self, kolor, marka, stan_aku):
        super().__init__(kolor, marka)
        self.stan_aku = stan_aku

    def czy_moge_wystartowac(self):
        return self.stan_aku > 20

class SamochodLPG(Samochod):
    def __init__(self, kolor, marka, stan_lpg):
        super().__init__(kolor, marka)
        self.stan_lpg = stan_lpg

    def czy_moge_wystartowac(self):
        return self.stan_lpg > 30

samochod1 = SamochodElektryczny("niebieski", "fiat", 20)
samochod2 = SamochodLPG("czarne", "bmw", 50)

samochod1.start()
samochod2.start()
