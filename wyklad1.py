class Samochod:

    def __init__(self, kolor, marka, paliwo):
        self.kolor = kolor
        self.marka = marka
        self.paliwo = paliwo

    def start(self):
        if self.__czy_mam_paliwo():
            print("Odpalam", self.marka, "koloru", self.kolor)
        else:
            print("Brak paliwa")

    def __czy_mam_paliwo(self):
        return self.paliwo > 30


samochod1 = Samochod("niebieski", "fiat", 20)
samochod2 = Samochod("czarne", "bmw", 50)

samochod1.start()
samochod2.start()
