class Samochod:

    def __init__(self, kolor, marka, paliwo):
        self.kolor = kolor
        self.marka = marka
        self.paliwo = paliwo

    def __eq__(self, other):
        return (self.kolor == other.kolor
                and self.marka == other.marka)

    def start(self):
        if self.__czy_moge_wystartowac():
            print("Odpalam", self.marka, "koloru", self.kolor)
        else:
            print("Brak paliwa")

    def __czy_moge_wystartowac(self):
        return self.paliwo > 30

    def __str__(self):
        opis = "kolor: {}; marka: {}; paliwo: {}".format(self.kolor, self.marka, self.paliwo)
        return opis

    @staticmethod
    def zamiana_jednostek_na_mile(kmh):
        return (kmh * 1)


samochod1 = Samochod("niebieski", "fiat", 20)
samochod2 = Samochod("czarne", "bmw", 50)
samochod3 = Samochod("niebieski", "fiat", 20)

samochod1.start()
samochod2.start()

x = Samochod.zamiana_jednostek_na_mile(50)
print("zamieniona: ", x)

x = samochod1.zamiana_jednostek_na_mile(50)
print("zamieniona: ", x)

print("samochod1:", samochod1)

if samochod1 == samochod3:
    print("taki sam")
else:
    print("różne")
